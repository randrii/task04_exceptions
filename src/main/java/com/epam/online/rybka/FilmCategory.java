package com.epam.online.rybka;

public enum FilmCategory {
    ACTION,
    COMEDY,
    DRAMA,
    HORROR,
    THRILLER,
    FANTASY;

    public String getCategoryName() {
        return this.name();
    }
}
