package com.epam.online.rybka;

public class UnsupportedDurationException extends Exception {
    public UnsupportedDurationException(String message) {
        super(message);
    }
}
