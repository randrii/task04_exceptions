package com.epam.online.rybka;

public class NoSuchGenreException extends Exception {
    public NoSuchGenreException(String message) {
        super(message);
    }
}
